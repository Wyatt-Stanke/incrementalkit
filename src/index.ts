interface Value {
    key: string
}

type Name = string | TemplateStringsArray
interface ExplicitOptions {
    key: string
}

type Options = Name | ExplicitOptions
const parseOpts = (opts: Options): ExplicitOptions => {
    if (Array.isArray(opts)) {
        return parseOpts(opts[0])
    } else if (typeof opts === "string") {
        return {key: opts}
    } else if (typeof opts === "") {
        return opts
    }
}

type Integer = number
const IntBase = (opts: ExplicitOptions): Integer => new class extends Number implements Value {
    key = opts.key
} as unknown as Integer

const int = o => IntBase(parseOpts(o))

let coins = int`coins`
coins += 2;